
import React, {useState} from "react";
import './App.css';
import {render} from "react-dom";
import  axios from "axios";


 const ConverterCurrency = ()=> {
     const [first, setFirst] = useState("USD");
     const [second, setSecond] = useState("RUB");
     const [inputCounter, setInputCounter] = useState("");
     const [inputCounter2, setInputCounter2] = useState("");
     let [rubCurrent, setRubCurrent] = useState(72.268042);
     let [usdCurrent, setUsdCurrent] = useState(0.013837);
     const [rate, setRate] = useState([]);
     let [rubValue, setRubValue] = useState("");
     let [usdValue, setUsdValue] = useState("");

     const getValue = () =>{
        setRubValue(inputCounter * rubCurrent)
    }
     const getValue2 = () =>{
         setUsdValue(inputCounter2 * usdCurrent)
     }


    const getRate = (first, second) => {
        axios({
            method: "GET",
            url:
                `https://free.currconv.com/api/v7/convert?q=${first}_${second}&compact=ultra&apiKey=67b7dbfbaed74c89ac11`,
        })
            .then((response)=>{
                console.log(response.data);
                setRate(response.data)
            })
            .catch((error)=> {
                console.log(error)
            })
    }

  return(
      <div>
      <div style={{marginLeft:"33%"}}>
          <div
              style={{
                  height:"150px",
                  width:"450px",
                  backgroundColor:"green",
                  display:"flex",
                  justifyContent: "center",
                  alignItems:"center",
                  fontSize:"25px"
              }}
          > 1 {first} = {rate[`${first}_${second}`]} {second}</div>
          <br/>
          <input type="text"
                 value={first}
                 onChange={(e)=>setFirst(e.target.value)}
          />
          <input type="text"
                 value={second}
          onChange={(e)=> setSecond(e.target.value)}/>
          <button
          onClick={()=> {
              getRate(first,second)
          }}>Convert</button>
          <br/>
          <br/>
          <br/>
          <input type="text"
                 value={inputCounter}
                 onChange={(e)=>setInputCounter(e.target.value)}
                 placeholder="USD"
          />
          <div>RUB:{rubValue}</div>
          <button
              onClick={()=> {
                  getValue()
              }}>Convert</button>
          <br/>
          <br/>
          <br/>
          <input type="text"
                 value={inputCounter2}
                 onChange={(e)=>setInputCounter2(e.target.value)}
                 placeholder="RUB"
          />
          <div>USD:{usdValue}</div>
          <button
              onClick={()=> {
                  getValue2()
              }}>Convert</button>

      </div>
      </div>
  )
};
render(<ConverterCurrency/>,document.querySelector("#root"))
export default ConverterCurrency



/*
useEffect(()=> {
    axios({
        method: "GET",
        url:
            "https://free.currconv.com/api/v7/convert?q=USD_RUB&compact=ultra&apiKey=67b7dbfbaed74c89ac11"
    })
        .then((response)=> {
            console.log(response.data)
            setUsdRub(response.data.USD_RUB)
        })
        .catch((error)=> {
            console.log(error)
        })
},[]);

 1 {first} = {rate[`${first}_${second}`]} {second}</div>*/
